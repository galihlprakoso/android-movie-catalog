package galih.uasgalih.Adapter;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import de.hdodenhof.circleimageview.CircleImageView;
import galih.uasgalih.Database.Helper.FavouriteMovieDBHelper;
import galih.uasgalih.DetailActivity;
import galih.uasgalih.DetailPosterActivity;
import galih.uasgalih.Fragment.NowPlayingFragment;
import galih.uasgalih.Fragment.UpcomingFragment;
import galih.uasgalih.MainActivity;
import galih.uasgalih.Model.Movie;
import galih.uasgalih.R;

/**
 * Created by Galih Laras Prakoso on 5/16/2018.
 */

public class MovieListAdapter extends RecyclerView.Adapter<MovieListViewHolder> {

    public ArrayList<Movie> listMovie;
    FavouriteMovieDBHelper db;
    Context context;
    View v;
    Object fragment;


    public MovieListAdapter(ArrayList<Movie> listMovie, Context context, View v, Object fragment) {
        db = new FavouriteMovieDBHelper(context,FavouriteMovieDBHelper.DATABASE_NAME,null,FavouriteMovieDBHelper.DATABASE_VERSION);
        this.context = context;
        this.listMovie = listMovie;
        this.v = v;
        this.fragment = fragment;
    }



    @NonNull
    @Override
    public MovieListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_movie_row, parent, false);

        return new MovieListViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull MovieListViewHolder holder, final int position) {

        final Movie m = listMovie.get(position);

        try{
            String potongJudul = "";

            if(m.getJudulFilm().length()>18){
                potongJudul = m.getJudulFilm().substring(0,18);
                potongJudul += "...";
                holder.judul.setText(potongJudul);
            }else{
                holder.judul.setText(m.getJudulFilm());
            }
        }catch(Exception e){
            holder.judul.setText(R.string.default_title);
        }

        try{
            String potongDeskripsi = "";

            if(m.getDeskripsiFilm().length()>80){
                potongDeskripsi = m.getDeskripsiFilm().substring(0,80);
                potongDeskripsi += "...";
                holder.deskripsi.setText(potongDeskripsi);
            }else{
                holder.deskripsi.setText(m.getDeskripsiFilm());
            }

        }catch(Exception e){
            holder.deskripsi.setText(R.string.description_default);
        }

        DateFormat dF = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date tgl = dF.parse(m.getTglRelaseFilm());
            SimpleDateFormat sDF = new SimpleDateFormat("dd, MMM yyyy", Locale.ENGLISH);

            String[] hari = new String[7];
            hari[1] = context.getString(R.string.monday);
            hari[2] = context.getString(R.string.tuesday);
            hari[3] = context.getString(R.string.wednesday);
            hari[4] = context.getString(R.string.thursday);
            hari[5] = context.getString(R.string.friday);
            hari[6] = context.getString(R.string.saturday);
            hari[0] = context.getString(R.string.sunday);

            holder.tglRelease.setText(hari[tgl.getDay()]+", "+sDF.format(tgl));
        } catch (ParseException e) {
            e.printStackTrace();
            holder.tglRelease.setText(R.string.date_release_default);
        }

        String url = new StringBuilder(m.getPoster()).deleteCharAt(0).toString();
        Glide.with(holder.context).load("http://image.tmdb.org/t/p/w185/"+url).into(holder.moviePoster);

        holder.btnFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if(!db.cekAdded(m.getJudulFilm())){
                HashMap<String,String> favMovie = new HashMap<>();

                favMovie.put(Movie.COL_JUDUL_FILM,m.getJudulFilm());
                favMovie.put(Movie.COL_POSTER,m.getPoster());
                favMovie.put(Movie.COL_DESKRIPSI_FILM,m.getDeskripsiFilm());
                favMovie.put(Movie.COL_TGL_RELEASE,m.getTglRelaseFilm());

                db.insert(favMovie);
                String addString = context.getString(R.string.is_added_to_favourite);
                Snackbar.make(v,m.getJudulFilm()+" "+addString,Snackbar.LENGTH_SHORT).show();
            }else{
                String addedString = context.getString(R.string.has_been_added);
                Snackbar.make(v,m.getJudulFilm()+" "+addedString,Snackbar.LENGTH_SHORT).show();
            }
            }
        });

        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fragment instanceof UpcomingFragment){
                    UpcomingFragment upcomingFragment = (UpcomingFragment)fragment;

                    Bundle bundle = new Bundle();
                    bundle.putParcelable("MOVIE",m);

                    Intent intent = new Intent(upcomingFragment.getActivity(), DetailActivity.class);
                    intent.putExtra("MOVIE",bundle);
                    intent.putExtra("ID_MOVIE",m.getID());

                    upcomingFragment.startActivity(intent);
                }else{
                    NowPlayingFragment nowPlayingFragment = (NowPlayingFragment)fragment;

                    Bundle bundle = new Bundle();
                    bundle.putParcelable("MOVIE",m);

                    Intent intent = new Intent(nowPlayingFragment.getActivity(), DetailActivity.class);
                    intent.putExtra("MOVIE",bundle);
                    Log.e("PUT","EXTRA : "+m.getID());
                    intent.putExtra("ID_MOVIE",m.getID());

                    nowPlayingFragment.startActivity(intent);
                }
            }
        });

        holder.moviePoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fragment instanceof UpcomingFragment){
                    UpcomingFragment upcomingFragment = (UpcomingFragment)fragment;

                    Intent intent = new Intent(upcomingFragment.getActivity(), DetailPosterActivity.class);
                    intent.putExtra("POSTER",m.getPoster());
                    intent.putExtra("TITLE",m.getJudulFilm());
                    upcomingFragment.startActivity(intent);
                }else{
                    NowPlayingFragment nowPlayingFragment = (NowPlayingFragment)fragment;

                    Intent intent = new Intent(nowPlayingFragment.getActivity(), DetailPosterActivity.class);
                    intent.putExtra("POSTER",m.getPoster());
                    intent.putExtra("TITLE",m.getJudulFilm());
                    nowPlayingFragment.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }
}

class MovieListViewHolder extends RecyclerView.ViewHolder{
    CircleImageView moviePoster;
    TextView judul;
    TextView deskripsi;
    TextView tglRelease;
    ImageButton btnFavourite;
    ImageButton btnDetail;
    Context context;
    View itemView;


    public MovieListViewHolder(View itemView,Context context) {
        super(itemView);
        this.context = context;
        this.itemView = itemView;
        moviePoster = (CircleImageView) itemView.findViewById(R.id.list_movie_row_movie_poster);
        judul = (TextView) itemView.findViewById(R.id.list_movie_row_judul_film);
        deskripsi = (TextView) itemView.findViewById(R.id.list_movie_row_deskripsi_film);
        tglRelease = (TextView) itemView.findViewById(R.id.list_movie_row_relase_film);
        btnFavourite = (ImageButton) itemView.findViewById(R.id.list_movie_row_btn_favourite);
        btnDetail = (ImageButton) itemView.findViewById(R.id.list_movie_row_btn_detail);
    }
}


