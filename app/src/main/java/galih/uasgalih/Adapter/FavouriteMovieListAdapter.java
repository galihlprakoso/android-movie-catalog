package galih.uasgalih.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import galih.uasgalih.Database.Helper.FavouriteMovieDBHelper;
import galih.uasgalih.DetailActivity;
import galih.uasgalih.DetailPosterActivity;
import galih.uasgalih.Fragment.FavouriteFragment;
import galih.uasgalih.Model.Movie;
import galih.uasgalih.R;

/**
 * Created by Galih Laras Prakoso on 5/16/2018.
 */

public class FavouriteMovieListAdapter extends RecyclerView.Adapter<FavouriteMovieListViewHolder> {

    public ArrayList<Movie> listMovie;
    FavouriteMovieDBHelper db;
    Context context;
    View v;
    FavouriteFragment f;


    public FavouriteMovieListAdapter(ArrayList<Movie> listMovie, Context context, View v, FavouriteFragment f) {
        db = new FavouriteMovieDBHelper(context,FavouriteMovieDBHelper.DATABASE_NAME,null,FavouriteMovieDBHelper.DATABASE_VERSION);
        this.context = context;
        this.listMovie = listMovie;
        this.v = v;
        this.f = f;
    }



    @NonNull
    @Override
    public FavouriteMovieListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_movie_favourite_row, parent, false);

        return new FavouriteMovieListViewHolder(itemView,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull FavouriteMovieListViewHolder holder, final int position) {

        final Movie m = listMovie.get(position);

        try{
            String potongJudul = "";

            if(m.getJudulFilm().length()>18){
                potongJudul = m.getJudulFilm().substring(0,18);
                potongJudul += "...";
                holder.judul.setText(potongJudul);
            }else{
                holder.judul.setText(m.getJudulFilm());
            }
        }catch(Exception e){
            holder.judul.setText(R.string.default_title);
        }

        try{
            String potongDeskripsi = "";

            if(m.getDeskripsiFilm().length()>80){
                potongDeskripsi = m.getDeskripsiFilm().substring(0,80);
                potongDeskripsi += "...";
                holder.deskripsi.setText(potongDeskripsi);
            }else{
                holder.deskripsi.setText(m.getDeskripsiFilm());
            }

        }catch(Exception e){
            holder.deskripsi.setText(R.string.description_default);
        }

        DateFormat dF = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date tgl = dF.parse(m.getTglRelaseFilm());
            SimpleDateFormat sDF = new SimpleDateFormat("dd, MMM yyyy", Locale.ENGLISH);

            String[] hari = new String[7];
            hari[1] = context.getString(R.string.monday);
            hari[2] = context.getString(R.string.tuesday);
            hari[3] = context.getString(R.string.wednesday);
            hari[4] = context.getString(R.string.thursday);
            hari[5] = context.getString(R.string.friday);
            hari[6] = context.getString(R.string.saturday);
            hari[0] = context.getString(R.string.sunday);

            holder.tglRelease.setText(hari[tgl.getDay()]+", "+sDF.format(tgl));
        } catch (ParseException e) {
            e.printStackTrace();
            holder.tglRelease.setText(R.string.date_release_default);
        }

        String url = new StringBuilder(m.getPoster()).deleteCharAt(0).toString();
        Glide.with(holder.context).load("http://image.tmdb.org/t/p/w185/"+url).into(holder.moviePoster);

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.delete(m.getID());
                f.getMovies();
                String deleteString = context.getString(R.string.is_delete_from_favourite);
                Snackbar.make(v,m.getJudulFilm()+deleteString,Snackbar.LENGTH_SHORT).show();
            }
        });

        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("MOVIE",m);

                Intent intent = new Intent(f.getActivity(), DetailActivity.class);
                intent.putExtra("MOVIE",bundle);

                f.startActivity(intent);
            }
        });

        holder.moviePoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(f.getActivity(), DetailPosterActivity.class);
                intent.putExtra("POSTER",m.getPoster());
                intent.putExtra("TITLE",m.getJudulFilm());
                f.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }
}

class FavouriteMovieListViewHolder extends RecyclerView.ViewHolder{
    CircleImageView moviePoster;
    TextView judul;
    TextView deskripsi;
    TextView tglRelease;
    ImageButton btnDelete;
    ImageButton btnDetail;
    Context context;


    public FavouriteMovieListViewHolder(View itemView,Context context) {
        super(itemView);
        this.context = context;
        moviePoster = (CircleImageView) itemView.findViewById(R.id.list_movie_favourite_movie_poster);
        judul = (TextView) itemView.findViewById(R.id.list_movie_favourite_judul_film);
        deskripsi = (TextView) itemView.findViewById(R.id.list_movie_favourite_deskripsi_film);
        tglRelease = (TextView) itemView.findViewById(R.id.list_movie_favourite_relase_film);
        btnDelete = (ImageButton) itemView.findViewById(R.id.list_movie_favourite_btn_delete);
        btnDetail = (ImageButton) itemView.findViewById(R.id.list_movie_favourite_btn_detail);
    }
}
