package galih.uasgalih.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Galih Laras Prakoso on 5/16/2018.
 */

public class Movie implements Parcelable {

    private String poster;
    private String judulFilm;
    private String deskripsiFilm;
    private String tglRelaseFilm;
    private String ID;

    protected Movie(Parcel in) {
        poster = in.readString();
        judulFilm = in.readString();
        deskripsiFilm = in.readString();
        tglRelaseFilm = in.readString();
    }

    public Movie(){

    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getPoster() {
        return poster;
    }

    public String getID(){ return ID; }

    public void setID(String ID){ this.ID = ID; }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getJudulFilm() {
        return judulFilm;
    }

    public void setJudulFilm(String judulFilm) {
        this.judulFilm = judulFilm;
    }

    public String getDeskripsiFilm() {
        return deskripsiFilm;
    }

    public void setDeskripsiFilm(String deskripsiFilm) {
        this.deskripsiFilm = deskripsiFilm;
    }

    public String getTglRelaseFilm() {
        return tglRelaseFilm;
    }

    public void setTglRelaseFilm(String tglRelaseFilm) {
        this.tglRelaseFilm = tglRelaseFilm;
    }

    public Movie(String poster, String judulFilm, String deskripsiFilm, String tglRelaseFilm) {
        this.poster = poster;
        this.judulFilm = judulFilm;
        this.deskripsiFilm = deskripsiFilm;
        this.tglRelaseFilm = tglRelaseFilm;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(poster);
        dest.writeString(judulFilm);
        dest.writeString(deskripsiFilm);
        dest.writeString(tglRelaseFilm);
    }

    //SQLite
    public static final String TABLE_NAME = "favourite_movie";
    public static final String COL_ID = "id";
    public static final String COL_POSTER = "poster_film";
    public static final String COL_JUDUL_FILM = "judul_film";
    public static final String COL_DESKRIPSI_FILM = "deskripsi_film";
    public static final String COL_TGL_RELEASE = "tgl_release_film";

    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                                                    + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                                                    + COL_JUDUL_FILM + " TEXT,"
                                                    + COL_POSTER + " TEXT,"
                                                    + COL_DESKRIPSI_FILM + " TEXT,"
                                                    + COL_TGL_RELEASE + " TEXT"
                                                    + ")";


}
