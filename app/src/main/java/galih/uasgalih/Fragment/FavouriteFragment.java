package galih.uasgalih.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import java.util.ArrayList;

import galih.uasgalih.Adapter.FavouriteMovieListAdapter;
import galih.uasgalih.Database.Helper.FavouriteMovieDBHelper;
import galih.uasgalih.Model.Movie;
import galih.uasgalih.R;


public class FavouriteFragment extends Fragment {

    View v;
    private ArrayList<Movie> movies;
    private RecyclerView lv;
    FavouriteMovieListAdapter movieListAdapter;
    FavouriteMovieDBHelper db;
    CardView noMovie;

    public FavouriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_favourite, container, false);

        db = new FavouriteMovieDBHelper(v.getContext(),FavouriteMovieDBHelper.DATABASE_NAME,null, FavouriteMovieDBHelper.DATABASE_VERSION);

        noMovie = v.findViewById(R.id.fragment_favourite_no_data);

        initFragment();

        return v;
    }

    private void initFragment() {
        movies = new ArrayList<>();
        lv = v.findViewById(R.id.fragment_favourite_playing_list_movie);
        movieListAdapter = new FavouriteMovieListAdapter(movies,getContext(),v,this);

        lv.setLayoutManager(new LinearLayoutManager(v.getContext(),LinearLayoutManager.VERTICAL,false));
        lv.setAdapter(movieListAdapter);

        getMovies();
    }

    public void getMovies() {
        movieListAdapter.listMovie = db.getAllMovies();

        if(movieListAdapter.listMovie.size()==0){
            noMovie.setVisibility(View.VISIBLE);
        }else{
            noMovie.setVisibility(View.GONE);
        }

        movieListAdapter.notifyDataSetChanged();
    }

    public void search(String search){
        movieListAdapter.listMovie = db.search(search);

        if(movieListAdapter.listMovie.size()==0){
            noMovie.setVisibility(View.VISIBLE);
        }else{
            noMovie.setVisibility(View.GONE);
        }

        movieListAdapter.notifyDataSetChanged();
    }

}
