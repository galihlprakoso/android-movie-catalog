package galih.uasgalih.Fragment;


import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;

import galih.uasgalih.Adapter.MovieListAdapter;
import galih.uasgalih.Config.MainConfig;
import galih.uasgalih.Helper.ReadJSON;
import galih.uasgalih.MainActivity;
import galih.uasgalih.Model.Movie;
import galih.uasgalih.R;

public class UpcomingFragment extends Fragment {

    View v;
    private ArrayList<Movie> movies;
    private RecyclerView lv;
    MovieListAdapter movieListAdapter;
    public MaterialDialog loading;
    UpcomingFragment fragment;
    CardView noInternet;

    public UpcomingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_upcoming, container, false);

        initFragment();

        return v;
    }

    private void initFragment() {
        fragment = this;

        noInternet = (CardView)v.findViewById(R.id.fragment_upcoming_no_internet);

        MaterialDialog.Builder builder = new MaterialDialog.Builder(v.getContext())
                .title(R.string.loading_title)
                .content(R.string.loading_content)
                .progress(true, 0).cancelable(false);

        loading = builder.build();

        movies = new ArrayList<>();
        lv = (RecyclerView) v.findViewById(R.id.fragment_upcoming_list_movie);
        movieListAdapter = new MovieListAdapter(movies,v.getContext(),v,this);

        lv.setLayoutManager(new LinearLayoutManager(v.getContext(),LinearLayoutManager.VERTICAL,false));
        lv.setAdapter(movieListAdapter);

        getMovies();
    }

    public void getMovies(){
        try {
            if(isInternetAvailable()){
                noInternet.setVisibility(View.GONE);

                this.getActivity().runOnUiThread(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
                    @Override
                    public void run() {
                    loading.show();
                    String url = "https://api.themoviedb.org/3/movie/upcoming?api_key="+MainConfig.API+"&language=en-US";
                    ReadJSON readJSON = new ReadJSON(movieListAdapter,loading);
                    readJSON.execute(url);
                    }
                });

            }else{
                noInternet.setVisibility(View.VISIBLE);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void search(final String query) {
        try {
            if(isInternetAvailable()){
                noInternet.setVisibility(View.GONE);

                getActivity().runOnUiThread(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
                    @Override
                    public void run() {
                    loading.show();
                    String newQuery = query;
                    newQuery = newQuery.replace(" ", "+");
                    String url = "https://api.themoviedb.org/3/search/movie?api_key=dbc78e6a0c563301453c1e0f6592d37b&query=" + newQuery;
                    new ReadJSON(movieListAdapter, loading).execute(url);
                    }
                });
            }else{
                noInternet.setVisibility(View.VISIBLE);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isInternetAvailable() throws InterruptedException, IOException
    {
        String command = "ping -c 1 google.com";
        return (Runtime.getRuntime().exec (command).waitFor() == 0);
    }



}
