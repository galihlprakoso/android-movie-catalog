package galih.uasgalih.Database.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import galih.uasgalih.Model.Movie;

/**
 * Created by Galih Laras Prakoso on 5/16/2018.
 */

public class FavouriteMovieDBHelper extends SQLiteOpenHelper {


    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "catalog_movie";

    public FavouriteMovieDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public FavouriteMovieDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Movie.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Movie.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    //CRUD
    public void insert(HashMap<String,String> favMovie){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Movie.COL_JUDUL_FILM,favMovie.get(Movie.COL_JUDUL_FILM));
        values.put(Movie.COL_POSTER,favMovie.get(Movie.COL_POSTER));
        values.put(Movie.COL_DESKRIPSI_FILM,favMovie.get(Movie.COL_DESKRIPSI_FILM));
        values.put(Movie.COL_TGL_RELEASE,favMovie.get(Movie.COL_TGL_RELEASE));

        long id = db.insert(Movie.TABLE_NAME,null,values);

        db.close();
    }

    public void delete(String ID){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(Movie.TABLE_NAME, Movie.COL_ID + " = ?",
                new String[]{ID});

        db.close();
    }

    public boolean cekAdded(String title){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(Movie.TABLE_NAME,
                new String[]{Movie.COL_ID,Movie.COL_JUDUL_FILM,
                        Movie.COL_POSTER, Movie.COL_DESKRIPSI_FILM,
                        Movie.COL_TGL_RELEASE},Movie.COL_JUDUL_FILM + "=?",
                new String[]{title},null,null,null,null);

        boolean result = cursor.getCount()!=0;

        cursor.close();

        return result;
    }

    public ArrayList<Movie> getAllMovies(){
        ArrayList<Movie> movies = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + Movie.TABLE_NAME + " ORDER BY " +
                Movie.COL_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Movie movie = new Movie();

                movie.setID(cursor.getString(cursor.getColumnIndex(Movie.COL_ID)));
                movie.setJudulFilm(cursor.getString(cursor.getColumnIndex(Movie.COL_JUDUL_FILM)));
                movie.setDeskripsiFilm(cursor.getString(cursor.getColumnIndex(Movie.COL_DESKRIPSI_FILM)));
                movie.setPoster(cursor.getString(cursor.getColumnIndex(Movie.COL_POSTER)));
                movie.setTglRelaseFilm(cursor.getString(cursor.getColumnIndex(Movie.COL_TGL_RELEASE)));

                movies.add(movie);
            }while (cursor.moveToNext());
        }

        db.close();
        cursor.close();

        return movies;
    }

    public ArrayList<Movie> search(String searchQuery) {

        ArrayList<Movie> movies = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor=db.rawQuery("SELECT * FROM "+ Movie.TABLE_NAME + " WHERE "
                 + Movie.COL_JUDUL_FILM +
                " LIKE  '%"+searchQuery+"%'",null);

        if(cursor.moveToFirst()){
            do{
                Movie movie = new Movie();

                movie.setID(cursor.getString(cursor.getColumnIndex(Movie.COL_ID)));
                movie.setJudulFilm(cursor.getString(cursor.getColumnIndex(Movie.COL_JUDUL_FILM)));
                movie.setDeskripsiFilm(cursor.getString(cursor.getColumnIndex(Movie.COL_DESKRIPSI_FILM)));
                movie.setPoster(cursor.getString(cursor.getColumnIndex(Movie.COL_POSTER)));
                movie.setTglRelaseFilm(cursor.getString(cursor.getColumnIndex(Movie.COL_TGL_RELEASE)));

                movies.add(movie);
            }while (cursor.moveToNext());
        }

        db.close();
        cursor.close();

        return movies;
    }


}
