package galih.uasgalih;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import galih.uasgalih.Config.MainConfig;
import galih.uasgalih.Helper.ReadJSONTrailer;
import galih.uasgalih.Model.Movie;

public class DetailActivity extends YouTubeBaseActivity implements
        YouTubePlayer.OnInitializedListener {
    private Movie m;
    private DetailActivity detailActivity;
    public String YOUTUBE_VIDEO_CODE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detail);

        detailActivity = this;

        Bundle b = getIntent().getBundleExtra("MOVIE");
        m = b.getParcelable("MOVIE");

        setTitle(m.getJudulFilm());

        ImageView poster = (ImageView)findViewById(R.id.activity_detail_poster);
        TextView title = (TextView) findViewById(R.id.activity_detail_title);
        TextView description = (TextView) findViewById(R.id.activity_detail_description);
        TextView tgl_release = (TextView) findViewById(R.id.activity_detail_tgl_release);

        try{
            title.setText(m.getJudulFilm());
        }catch(Exception e){
            title.setText(R.string.default_title);
        }

        try{
            description.setText(m.getDeskripsiFilm());
        }catch(Exception e){
            description.setText(R.string.description_default);
        }

        DateFormat dF = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date tgl = dF.parse(m.getTglRelaseFilm());
            SimpleDateFormat sDF = new SimpleDateFormat("dd, MMM yyyy", Locale.ENGLISH);

            String[] hari = new String[7];
            hari[1] = getString(R.string.monday);
            hari[2] = getString(R.string.tuesday);
            hari[3] = getString(R.string.wednesday);
            hari[4] = getString(R.string.thursday);
            hari[5] = getString(R.string.friday);
            hari[6] = getString(R.string.saturday);
            hari[0] = getString(R.string.sunday);

            tgl_release.setText(hari[tgl.getDay()]+", "+sDF.format(tgl));
        } catch (ParseException e) {
            e.printStackTrace();
            tgl_release.setText(R.string.date_release_default);
        }

        String url = new StringBuilder(m.getPoster()).deleteCharAt(0).toString();
        Glide.with(this).load("http://image.tmdb.org/t/p/w185/"+url).into(poster);

        poster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detailActivity, DetailPosterActivity.class);
                intent.putExtra("POSTER",m.getPoster());
                intent.putExtra("TITLE",m.getJudulFilm());
                startActivity(intent);
            }
        });

        TextView judulTrailer = (TextView)findViewById(R.id.activity_detail_judul_trailer);
        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.activity_detail_trailer);

        judulTrailer.setVisibility(View.GONE);
        youTubePlayerView.setVisibility(View.GONE);

        String idMovie = getIntent().getStringExtra("ID_MOVIE");

        Log.e("ID MOVIE","ID :"+idMovie);

        String urlTrailer = "http://api.themoviedb.org/3/movie/"+idMovie+"/videos?api_key="+ MainConfig.API;

        try{
            new ReadJSONTrailer(judulTrailer,youTubePlayerView,this).execute(urlTrailer);
        }catch (Exception e){
            judulTrailer.setVisibility(View.GONE);
            youTubePlayerView.setVisibility(View.GONE);
        }


    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            player.loadVideo(YOUTUBE_VIDEO_CODE);

        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, 1).show();
        } else {
            String errorMessage = String.format(
                    "ERROR : ", errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
