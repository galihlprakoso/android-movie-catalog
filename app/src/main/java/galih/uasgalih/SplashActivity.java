package galih.uasgalih;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

public class SplashActivity extends Activity {

    SplashActivity thisClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ImageView logo = findViewById(R.id.splash_logo);

        Glide.with(getBaseContext()).load(R.drawable.logo_movie_catalog)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(logo);

        thisClass = this;

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent main = new Intent(thisClass, MainActivity.class);
                startActivity(main);
                finish();
            }
        }, 3000);
    }
}
