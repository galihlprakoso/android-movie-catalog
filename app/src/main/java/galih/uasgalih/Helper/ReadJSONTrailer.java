package galih.uasgalih.Helper;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;

import android.view.View;
import android.widget.TextView;
import com.google.android.youtube.player.YouTubePlayerView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import galih.uasgalih.Config.MainConfig;
import galih.uasgalih.DetailActivity;

/**
 * Created by Galih Laras Prakoso on 5/16/2018.
 */

@RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
public class ReadJSONTrailer extends AsyncTask<String, Integer, String> {

    private TextView judulTrailer;
    private YouTubePlayerView youTubePlayerView;
    private DetailActivity detailActivity;

    public ReadJSONTrailer(TextView judulTrailer, YouTubePlayerView youTubePlayerView, DetailActivity detailActivity) {
        this.judulTrailer = judulTrailer;
        this.youTubePlayerView = youTubePlayerView;
        this.detailActivity = detailActivity;
    }

    @Override
    protected String doInBackground(String... strings) {
        return readURL(strings[0]);
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;
        try {

            jsonObject = new JSONObject(s);
            jsonArray = jsonObject.getJSONArray("results");

        } catch (JSONException e) {
//            e.printStackTrace();
            judulTrailer.setVisibility(View.GONE);
            youTubePlayerView.setVisibility(View.GONE);
        }

        try{
            judulTrailer.setText(jsonArray.getJSONObject(0).getString("name"));
            detailActivity.YOUTUBE_VIDEO_CODE = jsonArray.getJSONObject(0).getString("key");

            judulTrailer.setVisibility(View.VISIBLE);
            youTubePlayerView.setVisibility(View.VISIBLE);

            youTubePlayerView.initialize(MainConfig.DEVELOPER_KEY,detailActivity);

        }catch (Exception e){
            judulTrailer.setVisibility(View.GONE);
            youTubePlayerView.setVisibility(View.GONE);
        }
    }

    private static String readURL(String theUrl){
        StringBuilder content = new StringBuilder();
        try{
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while((line = bufferedReader.readLine())!=null){
                content.append(line+"\n");
            }
            bufferedReader.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return content.toString();
    }


}

