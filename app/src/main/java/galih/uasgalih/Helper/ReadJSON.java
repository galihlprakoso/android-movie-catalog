package galih.uasgalih.Helper;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import galih.uasgalih.Adapter.MovieListAdapter;
import galih.uasgalih.Model.Movie;

/**
 * Created by Galih Laras Prakoso on 5/16/2018.
 */

@RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
public class ReadJSON extends AsyncTask<String, Integer, String> {

    private MovieListAdapter movieListAdapter;
    private MaterialDialog loading;

    public ReadJSON(MovieListAdapter movieListAdapter, MaterialDialog loading) {
        this.movieListAdapter = movieListAdapter;
        this.loading = loading;
    }

    @Override
    protected String doInBackground(String... strings) {
        return readURL(strings[0]);
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {

            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("results");
            movieListAdapter.listMovie.clear();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject movieObject = jsonArray.getJSONObject(i);
                Movie movie = new Movie(
                        movieObject.getString("poster_path"),
                        movieObject.getString("title"),
                        movieObject.getString("overview"),
                        movieObject.getString("release_date")
                );
                movie.setID(movieObject.getString("id"));
                Log.e("IDMOVIE",movieObject.getString("id"));

                movieListAdapter.listMovie.add(movie);
            }
            movieListAdapter.notifyDataSetChanged();

            loading.dismiss();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static String readURL(String theUrl){
        StringBuilder content = new StringBuilder();
        try{
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while((line = bufferedReader.readLine())!=null){
                content.append(line+"\n");
            }
            bufferedReader.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return content.toString();
    }


}

