package galih.uasgalih;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class DetailPosterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detail_poster);

        String poster = getIntent().getStringExtra("POSTER");
        String title = getIntent().getStringExtra("TITLE");

        setTitle(title);

        ImageView imagePoster = (ImageView)findViewById(R.id.activity_detail_poster_poster);

        String url = new StringBuilder(poster).deleteCharAt(0).toString();
        Glide.with(this).load("http://image.tmdb.org/t/p/w185/"+url).into(imagePoster);
    }
}
