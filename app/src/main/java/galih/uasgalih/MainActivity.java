package galih.uasgalih;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.afollestad.materialdialogs.MaterialDialog;

import galih.uasgalih.Database.Helper.FavouriteMovieDBHelper;
import galih.uasgalih.Fragment.FavouriteFragment;
import galih.uasgalih.Fragment.NowPlayingFragment;
import galih.uasgalih.Fragment.UpcomingFragment;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    Toolbar toolbar;
    Object currentFragment;
    MenuItem menuSearch;
    MenuItem menuRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initAct();
    }

    private void initAct(){
        UpcomingFragment fragmentUpcoming = new UpcomingFragment();
        currentFragment = fragmentUpcoming;
        navigateToFragment(fragmentUpcoming,getString(R.string.title_upcoming));

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//         Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void navigateToFragment(Fragment fragment, String name){
        final FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
        setTitle(name);
        fragTrans.replace(R.id.app_content_container, fragment, name);
        fragTrans.commit();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        menuSearch = menu.findItem(R.id.action_search);
        menuRefresh = menu.findItem(R.id.action_refresh);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            search();
            return true;
        }else if(id==R.id.action_refresh){
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void search(){
        new MaterialDialog.Builder(this)
                .title(R.string.title_search)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(getString(R.string.type_your_search_query), null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        if(currentFragment instanceof UpcomingFragment){
                            UpcomingFragment fragment = (UpcomingFragment)currentFragment;

                            if(input!=null){
                                if(input.length()!=0){
                                    fragment.loading.show();
                                    fragment.search(input.toString());
                                }else{
                                    fragment.loading.show();
                                    fragment.getMovies();
                                }
                            }else{
                                fragment.loading.show();
                                fragment.getMovies();
                            }
                        }else if(currentFragment instanceof NowPlayingFragment){
                            NowPlayingFragment fragment = (NowPlayingFragment)currentFragment;

                            if(input!=null){
                                if(input.length()!=0){
                                    fragment.loading.show();
                                    fragment.search(input.toString());
                                }else{
                                    fragment.loading.show();
                                    fragment.getMovies();
                                }
                            }else{
                                fragment.loading.show();
                                fragment.getMovies();
                            }
                        }else if(currentFragment instanceof FavouriteFragment){
                            FavouriteFragment fragment = (FavouriteFragment)currentFragment;

                            if(input!=null){
                                if(input.length()!=0){
                                    fragment.search(input.toString());
                                }else{
                                    fragment.getMovies();
                                }
                            }else{
                                fragment.getMovies();
                            }
                        }
                    }

                }).show();
    }

    public void refresh(){
        if(currentFragment instanceof UpcomingFragment){
            UpcomingFragment fragment = (UpcomingFragment)currentFragment;
            fragment.loading.show();
            fragment.getMovies();
        }else if(currentFragment instanceof NowPlayingFragment){
            NowPlayingFragment fragment = (NowPlayingFragment)currentFragment;
            fragment.loading.show();
            fragment.getMovies();
        }else if(currentFragment instanceof FavouriteFragment){
            FavouriteFragment fragment = (FavouriteFragment)currentFragment;
            fragment.getMovies();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){
            case R.id.drawer_upcoming:
                menuSearch.setVisible(true);
                menuRefresh.setVisible(true);
                UpcomingFragment fragmentUpcoming = new UpcomingFragment();
                currentFragment = fragmentUpcoming;
                navigateToFragment(fragmentUpcoming,getString(R.string.title_upcoming));
                break;
            case R.id.drawer_now_playing:
                menuSearch.setVisible(true);
                menuRefresh.setVisible(true);
                NowPlayingFragment fragmentNowPlaying = new NowPlayingFragment();
                currentFragment = fragmentNowPlaying;
                navigateToFragment(fragmentNowPlaying,getString(R.string.title_now_playing));
                break;
            case R.id.drawer_favourite:
                menuSearch.setVisible(true);
                menuRefresh.setVisible(true);
                FavouriteFragment favouriteFragment = new FavouriteFragment();
                currentFragment = favouriteFragment;
                navigateToFragment(favouriteFragment,getString(R.string.title_favourite));
                break;
            case R.id.drawer_about:
                menuSearch.setVisible(true);
                menuRefresh.setVisible(true);
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;
            case R.id.drawer_setting:
                startActivity(new Intent(Settings.ACTION_LOCALE_SETTINGS));
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
